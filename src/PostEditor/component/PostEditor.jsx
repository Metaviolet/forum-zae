import React, { Component } from 'react';


class PostEditor extends Component {
	constructor(props) {
    super(props);

    // hack para asegurar que las funciones tiene el contexto del componente
    this.handlePostEditorInputChange = this.handlePostEditorInputChange.bind(this);
    this.createPost = this.createPost.bind(this);
    
    this.state = {
      newPostBody: ''
    }
  } 

  handlePostEditorInputChange(ev){
          this.setState({
          newPostBody: ev.target.value
        });
      }

  createPost (){
    this.props.addPost(this.state.newPostBody);
    this.setState({newPostBody: ''});
  }
  render () {
    return (
        <div className="panel-body"> 
             <textarea value={this.state.newPostBody} onChange={this.handlePostEditorInputChange} className="form-control post-editor-input" ></textarea>

             <button onClick={this.createPost} type="button" className="btn btn-success post-editor-button">Post</button>

        </div>
    )
  }    

}

export default PostEditor;     